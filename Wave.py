#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Wave - class defining set of minions in wave
'''

from M_caster import M_caster
from M_siege import M_siege
from M_super import M_super
from Minion import Minion


class Wave(object):

  def __init__(self, w_id):
    """ initiate default values """
    self.w_id = w_id
    self.buffs = []
    self.wave = []


  def __updateId(self, count = 1):
    """ update w_id counter """
    self.w_id += count


  def create(self, c_melee = 3, c_caster = 3, c_siege = 0, c_super = 0, cycle = 0):
    """ creates an array of minion objects """

    # create sieges
    if c_siege > 0:
      m_sieg = M_siege(self.w_id, cycle)
      sieges = m_sieg.create(c_siege)
      self.__updateId(c_siege)
    else:
      sieges = {}

    # create super
    if c_super > 0:
      m_sup = M_super(self.w_id, cycle)
      supers = m_sup.create(c_super)
      self.__updateId(c_super)
    else:
      supers = {}

    # create casters
    m_cast = M_caster(self.w_id, cycle)
    casters = m_cast.create(c_caster)
    self.__updateId(c_caster)

    # create melee
    m_mel = Minion(self.w_id, cycle)
    melee = m_mel.create(c_melee)
    self.__updateId(c_melee)

    # create dict and return
    result = sieges
    result.update(casters)
    result.update(melee)
    result.update(supers)

    self.wave = result
    return self


  def updateBuffs(self, name):
    """ updates buff list """
    if name not in self.buffs:
      self.buffs.append(name)


  def clearBuff(self, buff):
    """ clears all buffs """
    self.buffs.remove(buff)

