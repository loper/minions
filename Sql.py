import MySQLdb


class Base(object):
        '''
        MySQL connector and utility
        '''

        def __init__(self, host, user, password, db):
                ''' connects to db '''
                self._connection = MySQLdb.connect(host, user, password, db)
                self._connection.autocommit(True)
                self.locked = False
                self.cursor = None

        def disconnect(self):
                self._connection.close()

        def executeQuery(self, query, fetch=True):
                ''' executes SQL query,
                result: all rows '''
                if len(query) == 0:
                        return None    # empty query

                try:
                    if self.cursor is not None:
                            self.cursor.close()
                    self.cursor = self._connection.cursor()

                    self.cursor.execute(query)
                    if not self.locked:
                            self._connection.commit()    # if trans not locked
#                            self._connection.autocommit(True)
                    else:
                            print(query)
                            # self._connection.autocommit(False)
                    if fetch:
                            result = self.cursor.fetchall()
                            self.cursor.close()
                            return result
                except MySQLdb.IntegrityError as e:
                    print('[DATABASE] integrity error: {0}'.format(e))
                    print(query)
                    exit(1)
                except Exception as e:
                    print('[DATABASE] exception ({0}): {1}'.format(type(e), e))
                    print(query)
                    exit(1)
                self.cursor.close()

        def executeProc(self, proc, args, fetch=True):
                ''' executes SQL procedure,
                result: all rows '''
                if len(proc) == 0:
                    return None    # empty query

                try:
                    if self.cursor is not None:
                        self.cursor.close()
                    self.cursor = self._connection.cursor()

                    self.cursor.callproc(proc, args)

                    if fetch:
                            result = self.cursor.fetchall()
                            self.cursor.close()
                            return result
                except MySQLdb.IntegrityError as e:
                    print('[DATABASE] integrity error: {0}'.format(e))
                    exit(1)
                except Exception as e:
                    print('[DATABASE] exception ({0}): {1}'.format(type(e), e))
                    exit(1)
                self.cursor.close()

        def getAllRows(self, table):
                rows = self.executeQuery('SELECT * FROM {0}'.format(table))
                return rows

        def lockTransactions(self, lock=True):
                print('[DATABASE] dry run, transactions locked')
                self.locked = lock

        def commit(self):
            self._connection.commit()
