#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minion - class defining siege minion stats
'''

from Minion import Minion


class M_siege(Minion):

  def __init__(self, m_id, cycle = 0, cyc_divider = 2):
    """ initiate default values """
    super().__init__(m_id, cycle)
    self.m_id = m_id
    self.type = 'siege'

    self.health = 500
    self.damage = 40
    self.magic_damage = 10
    self.armor = 10 # %
    self.magic_resist = -30 # %
    self.crit_chance = 0  # %

    self.exposed = 0.8  # %
    self.every_wave = 3 # every 3rd wave

    # cycles
    self.c_health = 14
    self.c_damage = 3
    self.c_magic_damage = 1
    self.c_armor = 2.1
    self.c_magic_resistance = 0
    if cycle > 0:
      self.setCycle(cycle/cyc_divider-1)


