#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Buff - class defining buffs for minions
'''


class Buff(object):

  def __init__(self):
    """ initiate default values """
    self.timer_tick = 0
    self.duration = 1
    self.max_resist = 80
    self.normal = '\033[0;0m'
    self.color = self.normal
    pass


  def calcPercentage(self, value, percentage):
    """ calculate result value """
    return round(value * (100 + percentage) / 100)


  def reverseCalcPercentage(self, value, percentage):
    """ calculate result value """
    new_val = round(value * 100 / (100 + percentage))
    if new_val == 0:
      return value
    return new_val


  def updateBuffsList(self, waves_list):
    """ updates list of applied buffs """
    for wave in waves_list:
      wave.updateBuffs(self)


  def checkTimer(self, waves_list, timer):
    """ checks if buff out of duration """
    for wav in waves_list:
      for buff in wav.buffs:
        if timer > buff.timer_tick:
          buff.revertBuff(wav)


  def findBuff(self, wave, name):
    """ finds buff in wave and returns object """
    buffs_list = ([buff.name for buff in wave.buffs])
    if name in buffs_list:
      b_id = buffs_list.index(name)
      return wave.buffs[b_id]
    return None


  def applyBuff(self, waves_list, tick):
    """ applies buff for given wave """

    obj = self

    for wav in waves_list:
      applied = self.findBuff(wav, self.name)
      if applied is None:
        # buff is only applied when not already on
        for minion in wav.wave:
          if hasattr(self,'health'):
            wav.wave[minion].health = self.calcPercentage(wav.wave[minion].health, self.health)
          if hasattr(self,'damage'):
            wav.wave[minion].damage = self.calcPercentage(wav.wave[minion].damage, self.damage)
          if hasattr(self,'crit_chance'):
            wav.wave[minion].crit_chance = self.crit_chance
          if hasattr(self,'magic_damage'):
            wav.wave[minion].magic_damage = self.calcPercentage(wav.wave[minion].magic_damage, self.magic_damage)
          if hasattr(self,'magic_resistance'):
            wav.wave[minion].magic_resistance = self.calcPercentage(wav.wave[minion].magic_resistance, self.magic_resistance)
            wav.wave[minion].magic_resistance = min(wav.wave[minion].magic_resistance,self.max_resist)
          if hasattr(self,'armor'):
            wav.wave[minion].armor = self.calcPercentage(wav.wave[minion].armor, self.armor)
            wav.wave[minion].armor = min(wav.wave[minion].armor,self.max_resist)
        obj = self
      else:
        obj = applied

      obj.updateBuffsList(waves_list)

    # set or update timer for duration control
    obj.timer_tick += self.duration + tick


  def revertBuff(self, wav):
    """ checks if buff out of duration """
    for minion in wav.wave:
      if hasattr(self,'health'):
        wav.wave[minion].health = self.reverseCalcPercentage(wav.wave[minion].health, self.health)
      if hasattr(self,'damage'):
        wav.wave[minion].damage = self.reverseCalcPercentage(wav.wave[minion].damage, self.damage)
      if hasattr(self,'magic_damage'):
        wav.wave[minion].magic_damage = self.reverseCalcPercentage(wav.wave[minion].magic_damage, self.magic_damage)
      if hasattr(self,'crit_chance'):
        wav.wave[minion].crit_chance = 0
      if hasattr(self,'armor'):
        wav.wave[minion].armor = self.reverseCalcPercentage(wav.wave[minion].armor, self.armor)
        wav.wave[minion].armor = min(wav.wave[minion].armor,self.max_resist)
      if hasattr(self,'magic_resistance'):
        wav.wave[minion].magic_resistance = self.reverseCalcPercentage(wav.wave[minion].magic_resistance, self.magic_resistance)
        wav.wave[minion].magic_resistance = min(wav.wave[minion].magic_resistance,self.max_resist)
    wav.clearBuff(self)
    self.color = self.normal



# ------------------------------------------------------------

class RedBuff(Buff):

  def __init__(self):
    """ sets red buff """
    super().__init__()
    self.damage = 170 # + %
    self.crit_chance = 60  # %
    # 150 x 40 -> avg 36.190935
    self.duration = 10
    self.color = '\033[1;31m'
    self.name = 'red'


class BlueBuff(Buff):

  def __init__(self):
    """ sets blue buff """
    super().__init__()
    self.magic_damage = 80 # + %
    self.magic_resistance = 20 # + %
    self.crit_chance = 30  # %
    # 60 x 80 x 20 -> avg 61.928642
    self.duration = 20
    self.color = '\033[1;34m'
    self.name = 'blue'


class SilverBuff(Buff):

  def __init__(self):
    """ sets silver (armor) buff """
    super().__init__()
    self.armor = 15 # +%
    self.duration = 15
    self.color = '\033[1;30m\033[47m'
    self.name = 'silver'


class AtomicDebuff(Buff):

  def __init__(self):
    """ sets atomic bomb de-buff """
    super().__init__()
    self.health = -50 # %
    self.damage = 500 # %
    self.duration = 50
    self.name = 'atomic'

