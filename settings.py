# settings for Minions (start.py)

CLIENT_VER = '0.3.3'
CLIENT_TYPE = 'pbe' # pbe/pbe/prod

sleep_time = .2
upgrade_every = 15 # upgrade every n waves
siege_every = 5 # spawn siege minion every n waves
wave_size = 6 # all minions count in wave
wave_limit = 20 # max waves

# balance: blue [%%%%%%%%########] purple
#              0%      50%     100%
balance_typical = 35 # typical attacks per timer (min 40)
balance_change = 1 # balance progress change

# waves containers
waves_blue = []
waves_purple = []

# turrets
turr_positions = [5, 20, 40, 60, 80, 95]
turr_range = 2
