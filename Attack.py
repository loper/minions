#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Attack - class defining battle algorithms
'''

import random
from copy import deepcopy


class Attack(object):

  def __init__(self):
    """ initiate default values """
    self.multiplier = 10 # it may need to be changed for
                         # probs (probabilities) < 0.1, i.e. 0.05, 0.02


  def pick_random(self, wave):
    """ pick one from wave, according to their probability """

    # list of probabilities
    probs = [ [prob.exposed, prob.m_id] for prob in wave.values() ]

    # group by value
    values = set([val[0] for val in probs])
    groups = [{x: [y[1] for y in probs if y[0]==x]} for x in values]

    # pick from every group
    picked = {}
    for group in groups:
      for key in group.keys():
        choosen = random.choice(group[key])
        picked.update( {key: choosen} )

    # duplicate values
    pool = []
    for pick in picked.keys():
      multiplier = int(self.multiplier * pick)
      pool.extend([picked[pick]] * multiplier)

    # choose one
    one = random.choice(pool)
    return wave[one]  # return minion object


#  def old_remove_items(self, items_list, waves_list):
#    remaining = deepcopy(items_list)
#    for wl in waves_list:
#      for key in items_list:
#        if key in wl.wave:
#          wl.wave.pop(key)
#          remaining.remove(key)
#
#        # remove empty wave object from list
#        if len(wl.wave) == 0:
#          waves_list.remove(wl)
#    return remaining
#
#
#  def old_remove_corpses(self, one_wave, second_wave, blue_wave, purple_wave):
#    """ remove all dead minions from wave """
#
#    wave = deepcopy(one_wave)
#    wave.update(deepcopy(second_wave))
#
#    to_remove = []
#
#    for key in wave.keys():
#      if wave[key].health <= 0:
#        # print('Minion {0} has been slained'.format(wave[key].m_id))
#        to_remove.append(key)
#
#    if len(to_remove) > 0:
#      to_remove = self.remove_items(to_remove, blue_wave)
#      print(to_remove)
#      to_remove = self.remove_items(to_remove, purple_wave)
#      print(to_remove)


  def remove_corpses(self, waves):
    """ remove all dead minions from wave """
    to_remove=[]
    for minions in waves:
      for key, minion in minions.wave.items():
        if minion.health <= 0:
          to_remove.append((minions, key))

    for wav, key in to_remove:
      # remove dead minions
      wav.wave.pop(key)

      # remove empty waves
      if len(wav.wave) == 0:
        if wav in waves:
          waves.remove(wav)
    return waves



  def damage_minion(self, attacker, victim):
    """ update values according to damage and armors """
    # attacker parameters
    dmg = attacker.damage
    magic_dmg = attacker.magic_damage
    crit = attacker.crit_chance

    # XXX error
    if dmg <= 0 and magic_dmg <= 0:
      import pdb
      pdb.set_trace()

    # victim parameters
    hp = victim.health
    armor = min(80,victim.armor)
    magic_res = min(80,victim.magic_resistance)

    # calculate total damage - reflected values are in %
    # total_dmg = (dmg - armor/100) + (magic_dmg - magic_res/100)
    total_dmg = (dmg - dmg*armor/100) + (magic_dmg - magic_dmg*magic_res/100)

    if (magic_res > 100 or armor > 100):
      print('error: resistance over 100%')
      import pdb
      pdb.set_trace()

    # add critical hit
    rand = random.random()
    if rand <= crit/100:
      total_dmg *= 2  # 200%

    # decrease health
    if total_dmg < 0:
      print('error: minus damage: {}'.format(total_dmg))
      import pdb
      pdb.set_trace()

    hp -= round(total_dmg) # rounded to full int
    victim.health = hp



  def one_battle(self, one, two, i):
    """ one battle between waves """
    try:
      minion = tuple(one.values())[i]
      opponent = self.pick_random(two)
      self.damage_minion(minion, opponent)

    except IndexError:
      #print('out of range')
      pass
    return (one, two)



  def fight(self, blue_wave, purple_wave, rand = True):
    """ start a battle between waves """

    # get all minions from waves
    blue_minions = {}
    for minions in blue_wave:
      blue_minions.update(minions.wave)
    purple_minions = {}
    for minions in purple_wave:
      purple_minions.update(minions.wave)

    one = blue_minions
    two = purple_minions
    if rand:
      # for justice, randomly pick who starts
      if random.randint(1,3) == 1:
        one = purple_minions
        two = blue_minions


    # loop through bigger wave and ignore missing
    for i in range(0, max(len(one),len(two))):
      # wave one
      one, two = self.one_battle(one, two, i)
      # wave two
      two, one = self.one_battle(two, one, i)

      # clear dead
      self.remove_corpses(blue_wave)
      self.remove_corpses(purple_wave)


