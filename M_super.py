#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minion - class defining super minion stats
'''

from Minion import Minion


class M_super(Minion):

  def __init__(self, m_id = 0, cycle = 0):
    """ initiate default values """
    super().__init__(m_id, cycle)
    self.m_id = m_id
    self.type = 'caster'

    self.health = 1500
    self.damage = 190
    self.magic_damage = 25
    self.armor = 30 # %
    self.magic_resist = -30 # %

    self.crit_chance = 0  # %
    self.exposed = 0.9  # %

    # cycles
    self.c_health = 15
    self.c_damage = 17
    self.c_magic_damage = 2
    self.c_armor = 0.75
    self.c_magic_resistance = 0
    if cycle > 0:
      self.setCycle(cycle)

