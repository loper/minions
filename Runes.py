#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Runes - class defining runes
'''

from user import rune_page

from sys import exit
import itertools
import pdb


class Runes(object):

  def __init__(self):
    """ initiate default values """
    # count limits
    self.max_quints = 3
    self.max_marks = 9
    self.max_seals = 9
    self.max_glyphs = 9


  def prepareData(self, connection, all_runes):
    """ check runes and generate pages """
    self.connection = connection

    self.all_runes = all_runes # all available runes
    self.checkRunes(rune_page, all_runes)
    self.rune_page = self.generateRunesPage(rune_page)


  def checkRunes(self, rune_page, all_runes):
    """ check if user picked available runes """
    user_chosen = [[r[0]]*r[1] for r in rune_page]
    user_chosen = list(itertools.chain(*user_chosen))
    if len(user_chosen) == 0: return
    available = [[r[1]]*r[2] for r in all_runes]
    available = list(itertools.chain(*available))

    # check if available runes were chosen
    for rune in user_chosen:
      try:
        available.remove(rune)
      except ValueError:
        print('[ERROR] User has chosen not available runes')
        exit(20)

    # check if out of max range
    if len([c for c in user_chosen if c.startswith('quint')]) > self.max_quints:
      print('[ERROR] User has chosen too many quintessences')
      exit(21)
    if len([c for c in user_chosen if c.startswith('mark')]) > self.max_marks:
      print('[ERROR] User has chosen too many marks')
      exit(22)
    if len([c for c in user_chosen if c.startswith('seal')]) > self.max_seals:
      print('[ERROR] User has chosen too many seals')
      exit(23)
    if len([c for c in user_chosen if c.startswith('glyph')]) > self.max_glyphs:
      print('[ERROR] User has chosen too many glyphs')
      exit(24)


  def generateRunesPage(self, list_of_runes):
    """ generate runes page from given list """
    rune_page = []

    quints_list = [q for q in list_of_runes if q[0].startswith('quint')]
    for quint in quints_list:
      quint_list = [quint[0]] * quint[1]
      rune_page = self.__createRunesPage(rune_page, quints = quint_list)

    marks_list = [m for m in list_of_runes if m[0].startswith('mark')]
    for mark in marks_list:
      mark_list = [mark[0]] * mark[1]
      rune_page = self.__createRunesPage(rune_page, marks = mark_list)

    seals_list = [s for s in list_of_runes if s[0].startswith('seal')]
    for seal in seals_list:
      seal_list = [seal[0]] * seal[1]
      rune_page = self.__createRunesPage(rune_page, seals = seal_list)

    glyphs_list = [g for g in list_of_runes if g[0].startswith('glyph')]
    for glyph in glyphs_list:
      glyph_list = [glyph[0]] * glyph[1]
      rune_page = self.__createRunesPage(rune_page, glyphs = glyph_list)

    return rune_page


  def __createRunesPage(self, rune_page = [], quints = [], marks = [], seals = [], glyphs = []):
    """ craete collection of runes """
    for q in quints:
      quint = Quint(q)
      rune_page.append(quint)
      self.setParams(quint)
    for m in marks:
      mark = Mark(m)
      rune_page.append(mark)
      self.setParams(mark)
    for s in seals:
      seal = Seal(s)
      rune_page.append(seal)
      self.setParams(seal)
    for g in glyphs:
      glyph = Glyph(g)
      rune_page.append(glyph)
      self.setParams(glyph)
    return rune_page


  def applyRunes(self, wave, cycle):
    """ apply runes to all minions in wave """
    for key in wave.wave.keys():
      minion = wave.wave[key]
      for rune in self.rune_page:
        if hasattr(rune,'health'):
          minion.health += rune.health
        if hasattr(rune,'c_health'):
          minion.health += rune.c_health * cycle
        if hasattr(rune,'damage'):
          if minion.damage > 0:
            minion.damage += rune.damage
        if hasattr(rune,'c_damage'):
          if minion.damage > 0:
            minion.damage += rune.c_damage * cycle
        if hasattr(rune,'magic_damage'):
          if minion.magic_damage > 0:
            minion.magic_damage += rune.magic_damage
        if hasattr(rune,'c_magic_damage'):
          if minion.magic_damage > 0:
            minion.magic_damage += rune.c_magic_damage * cycle
        if hasattr(rune,'armor'):
          minion.armor += rune.armor
        if hasattr(rune,'c_armor'):
          minion.armor += rune.c_armor * cycle
        if hasattr(rune,'magic_resistance'):
          minion.magic_resistance += rune.magic_resistance
        if hasattr(rune,'c_magic_resistance'):
          minion.magic_resistance += rune.c_magic_resistance * cycle


  def setParams(self, rune):
    """ set rune params from DB """
    if rune.__class__ == Quint:
      db_name = 'r_quints'
    elif rune.__class__ == Mark:
      db_name = 'r_marks'
    elif rune.__class__ == Seal:
      db_name = 'r_seals'
    elif rune.__class__ == Glyph:
      db_name = 'r_glyphs'
    query = "SELECT * FROM {} WHERE name = '{}'".format(db_name, rune.name)
    data = self.connection.executeQuery(query)
    data = data[0]
    if data[3] is not None:
      rune.health = data[3]
    if data[4] is not None:
      rune.c_health = data[4]
    if data[5] is not None:
      rune.damage = data[5]
    if data[6] is not None:
      rune.c_damage = data[6]
    if data[7] is not None:
      rune.magic_damage = data[7]
    if data[8] is not None:
      rune.c_magic_damage = data[8]
    if data[9] is not None:
      rune.armor = data[9]
    if data[10] is not None:
      rune.c_armor = data[10]
    if data[11] is not None:
      rune.magic_resistance = data[11]
    if data[12] is not None:
      rune.c_magic_resistance = data[12]


# ------------------------------

class Quint(Runes):
  def __init__(self, name = None):
    """ initiate default values """
    super().__init__()
    if name is not None:
      self.name = name

class Mark(Runes):
  def __init__(self, name = None):
    """ initiate default values """
    super().__init__()
    if name is not None:
      self.name = name

class Seal(Runes):
  def __init__(self, name = None):
    """ initiate default values """
    super().__init__()
    if name is not None:
      self.name = name

class Glyph(Runes):
  def __init__(self, name = None):
    """ initiate default values """
    super().__init__()
    if name is not None:
      self.name = name


# http://leagueoflegends.wikia.com/wiki/List_of_runes

# XXX pamietac, zeby nie dawac bonusu, jak minion ma 0 w statsie - melee nie moze atakowac magia, itp.
# XXX tylko esencje powinny uwzgledniac wszystkie statsy, cala reszta opowiednio - znaki: atak,
#     pieczecie - obrona/zdrowie, glify - magia/zdrowie

