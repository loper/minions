## Special offer 1.09.2014 - 5.09.2014 ##

Stay healthy. Now all health runes are **30% off**. Limited time only.

# Important announcement #

Due to work on completly new mechanism based on client - server communication, we're suspending current work on new features and patches for PBE (obviously bugfixes will come but nothing new).

Current attention is focused on separating battle clockwork and database operations (serverside) from only showing progress and connecting (clientside). This will move heavy loads away from client machine and ease permissions management for operators and admins. All based on current fight-engine.

We hope this new feature will gave opportunity for making a whole new multiplayer mode, maybe even with ranked games allowed, will see.

Good luck on PBE!


## Also please check out Wiki for some information how things work: ##

## Minion stats & abilities ##

* [Melee](minions/wiki/Melee)

* [Caster](minions/wiki/Caster)

* [Siege](minions/wiki/Siege)

* [Super](minions/wiki/Super)

## [Buffs and gains for minions](minions/wiki/Buffs) ##

* red buff

* blue buff

* silver buff

## [Turret mechanisms](minions/wiki/Turrets) ##

## [Ranking system](minions/wiki/League%20of%20Minions) ##

## [PBE stats](minions/wiki/PBEstats) ##