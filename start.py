#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minions - simulate LoL minions spawns and fights
by Loper
'''

import Buff
from Wave import Wave
from Attack import Attack
from Turret import Turret
from Login import Login
from Runes import Runes
from show import *

from time import sleep
from sys import exit
import random
import math

import pdb # XXX


# SETTINGS
from settings import *

# buffs
buffChecker = Buff.Buff()

# DB login
login = None

# turrets
turr = None

def debugHealths():
  blue = waves_blue
  purple = waves_purple
  blue=[k.wave for k in blue]
  purple=[k.wave for k in purple]
  z={}
  for b in blue:
    z.update(b)
  print([v.health for k,v in z.items()], end='')
  print(' / ', end = '')
  z={}
  for b in purple:
    z.update(b)
  print([v.health for k,v in z.items()])


def debugArmor():
  blue = waves_blue
  purple = waves_purple
  blue=[k.wave for k in blue]
  purple=[k.wave for k in purple]
  z={}
  for b in blue:
    z.update(b)
  print([v.armor for k,v in z.items()], end='')
  print(' / ', end = '')
  z={}
  for b in purple:
    z.update(b)
  print([v.armor for k,v in z.items()])


def debugDamage():
  blue = waves_blue
  purple = waves_purple
  blue=[k.wave for k in blue]
  purple=[k.wave for k in purple]

  z={}
  for b in blue:
    z.update(b)
  damages = [v.damage for k,v in z.items()]
  magic = [v.magic_damage for k,v in z.items()]
  damages = [x+y for x,y in zip(damages, magic)]
  print(damages, end='')

  print(' / ', end = '')

  z={}
  for b in purple:
    z.update(b)
  damages = [v.damage for k,v in z.items()]
  magic = [v.magic_damage for k,v in z.items()]
  damages = [x+y for x,y in zip(damages, magic)]
  print(damages)

  print('')


def createWave(starting, cycle, siege = 0, super_m = 0):
  """ create wave and update old """
  wave = Wave(starting)
  new_wave = wave.create(cycle = cycle, c_siege = siege, c_super = super_m)
  return wave


def updateSpawnTimes(spawn_times, balance):
  """ update spawn times """
  spawn_times[0] = round(balance_typical * balance / 100)
  spawn_times[1] = balance_typical - spawn_times[0]

  # boundaries
  bound = 5
  bound_add = bound - 1
  if(spawn_times[0]) <= bound:
    spawn_times[0] += bound_add
  if(spawn_times[1]) <= bound:
    spawn_times[1] += bound_add

  return spawn_times


def checkTimer(timer, blue, purple, starting, turr, turrets):
  """ check timer and spawn wave """
  cycle = round(timer / upgrade_every)

  applyRandomBuff(timer, spawn_times, blue, purple)

  siege = 0
  super_m = 0
  # spawn siege every n waves
  if timer % siege_every == 0: siege = 1

  spawned = False
  # create next waves
  if (spawn_times[0] == 0 or timer % spawn_times[0] == 0):
    # blue
    # spawn super if 1 turret left
    if len([t for t in turrets if t.position > 50]) == 1:
      super_m = 1
      siege = 0

    # check wave count limit
    if len(blue) < wave_limit:
      new_wave = createWave(starting, cycle, siege, super_m)
      if login.team == 'blue': runes.applyRunes(new_wave, cycle)
      waves_blue.append(new_wave)
      spawned = True

  if (spawn_times[1] == 0 or timer % spawn_times[1] == 0):
    # purple
    if len([t for t in turrets if t.position < 50]) == 1:
      super_m = 1
      siege = 0

    # check wave count limit
    if len(purple) < wave_limit:
      new_wave = createWave(starting, cycle, siege, super_m)
      if login.team == 'purple': runes.applyRunes(new_wave, cycle)
      waves_purple.append(new_wave)
      spawned = True

  if spawned:
    # scale turrets
    turr.scaleAll(turrets)

  # apply atomic debuff to limit overpowered waves
  if len(blue) == wave_limit and len(purple) == wave_limit:
    buff = Buff.AtomicDebuff()
    buff.applyBuff(waves_blue, timer)
    buff = Buff.AtomicDebuff()
    buff.applyBuff(waves_purple, timer)


def applyRandomBuff(timer, spawn_times, blue, purple):
  """ applies randomly choosen buff for random wave """
  chance = 65 # %
  every = 5 # every timer tick

  # modify chance according to value
  every = min(spawn_times)

  if ( timer % every ) == 0:
    # check timers
    buffChecker.checkTimer(waves_blue, timer)
    buffChecker.checkTimer(waves_purple, timer)

    rand = random.random()
    if rand <= chance/100:
      blue_chance = round(spawn_times[1]/(spawn_times[0]+spawn_times[1])*100)

      rand = random.random()

      # pick team
      if rand <= blue_chance/100:
        waves_list = waves_blue
      else:
        waves_list = waves_purple

      # pick buff
      which = random.randint(1,3)
      if which == 1:
        buff = Buff.RedBuff()
        wave=buff.applyBuff(waves_list, timer)
      elif which == 2:
        buff = Buff.SilverBuff()
        wave=buff.applyBuff(waves_list, timer)
      else:
        buff = Buff.BlueBuff()
        wave=buff.applyBuff(waves_list, timer)



def calcPoints(timer, waves, turrets, balance):
  """ calculate points for teams """
  timer_p = (0.013 * timer + 8)

  waves_blue = len(waves[0])
  waves_purple = len(waves[1])
  if waves_blue > 0:
    waves_blue = (6.56 * math.pow(math.e, (-0.1 * waves_blue)))
  if waves_purple > 0:
    waves_purple = (6.56 * math.pow(math.e, (-0.1 * waves_purple)))

  b_t = len([t.position for t in turrets if t.position < 50])
  p_t = len([t.position for t in turrets if t.position > 50])
  blue_turrets = (0.33 * pow(b_t,3) - 1.5 * pow(b_t,2) + 3.14 * b_t + 1)
  purple_turrets = (0.33 * pow(p_t,3) - 1.5 * pow(p_t,2) + 3.14 * p_t + 1)

  blue_won = (1 * 20) if balance > 90 else 0
  purple_won = (1  * 20) if balance < 10 else 0

  rand_w = random.randint(-4, 4)

  blue_p = round(timer_p + waves_blue + blue_turrets + blue_won + rand_w)
  purple_p = round(timer_p + waves_purple + purple_turrets + purple_won + rand_w)
  return blue_p, purple_p



def startBattle(timer, balance, spawn_times, runes):
  """ create waves and start the battle """
  # spawn turrets
  turr = Turret()
  turrets = turr.createTurrets(turr_positions, turr_range)

  # create initial waves
  starting = 1
  blue = createWave(starting, 0)
  purple = createWave(len(blue.wave)+starting, 0)

  # apply initial wave runes
  if login.team == 'blue': runes.applyRunes(blue, 0)
  elif login.team == 'purple': runes.applyRunes(purple, 0)

  # add initial waves to list
  waves_blue.append(blue)
  waves_purple.append(purple)

  # start the battle
  for i in range(2,5000):

    # starting value
    starting = wave_size*i+1

    # begin fight -------
    while True:

      # wait for a while
      try:
        sleep(sleep_time)
      except KeyboardInterrupt:
        # pdb.set_trace()
        exit(95)

      # show progress
      showData(waves_blue, waves_purple, balance, turrets, timer)

      # check, if timer hit spawn time
      timer += 1
      checkTimer(timer, waves_blue, waves_purple, starting, turr, turrets)
      if timer > 5000: exit(6)

      # check if turret in range and attack
      passed=turr.attack(turrets, waves_blue, waves_purple, balance)

      # check, if end of wave
      if (len(waves_blue) == 0 and balance >= 50): break
      if (len(waves_purple) == 0 and balance <= 50): break
      if (passed and len(waves_blue) == 0 or len(waves_purple) == 0): break


      # initiate one fight
      battle = Attack()
      battle.fight(waves_blue, waves_purple)

    # end fight ----------

    if (len(waves_blue) == 0):
      # extra check for passing the turret
      pos = [t.position for t in turrets if t.position < 50]
      if len(pos) == 0 or pos[-1] < balance:
        balance -= balance_change
    else:
      # extra check for passing the turret
      pos = [t.position for t in turrets if t.position > 50]
      if len(pos) == 0 or pos[0] > balance:
        balance += balance_change

    # update spawn times
    spawn_times = updateSpawnTimes(spawn_times, balance)

    # balance overflow - game over
    if balance >= 100 or balance <= 0:
      # show progress
      showData(waves_blue, waves_purple, balance, turrets, timer)

      # calc points
      blue_p, purple_p = calcPoints(timer, [waves_blue, waves_purple], turrets, balance)
      login.addMatch([blue_p, purple_p], balance, timer)
      login.updatePoints([blue_p, purple_p])

      # show some stats
      print('')
      print('')
      print('Match results - blue team: {0} points, purple team: {1} points'.format(blue_p, purple_p))
      print('Match time: {0}'.format(timer))
      print('')
      login.showResult(balance)

      # clean on exit
      login.disconnect()
      exit((balance+100) % 99)



if __name__ == '__main__':

  # login to database
  login = Login(CLIENT_VER, CLIENT_TYPE)

  # get runes data
  user_runes = login.loadRunes()

  runes = Runes()
  runes.prepareData(login.connection, user_runes)

  # timer per attack
  timer = 0

  # starting balance %
  balance = 50

  # spawn times according to balance
  spawn_times = [ 15, 15 ]
  spawn_times = [ 23, 23 ]

  # start the battle
  startBattle(timer, balance, spawn_times, runes)

  # clean on exit
  login.disconnect()
  exit(balance)
