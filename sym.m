clc
clear all;

% settings

rd = 9; % ranged damage
md = 7; % melee damage
rh = 50; % ranged health
mh = 60; % melee health
ws = 8; % wave size
rmr = 3; % ranged to melee ratio

% buffs
% crit_chance
% heal per sec

% functions -----------------

function [ ad ] = attackDamage(mv, rd, md, rmr)
  count = size(mv, 2);
  ranged = nnz( mv(1 : rmr) ) * rd;
  melee = nnz( mv(rmr+1 : count) ) * md;
  ad = ranged + melee;
end


function [ dv ] = damageVector(mv) % mv - minion wave vector, dv - damage vector
  ws = size(mv, 2);
  dv = zeros(1, ws); % damage vector

  count = nnz(mv);
  while (count > 0)
    r = randi(ws);
    if ( mv(r) > 0 )
      dv(r) = dv(r) + 1;
      count = count - 1;
    end
  end
end


function [ dmv ] = attack(mv, ad, dv) % dmv - damaged minion wave
  sd = ad / nnz(mv); % split damage over minion wave
  dmv = mv - (dv * sd);
end


function [ cmv ] = correctValues(mv) % cmv - correct minion wave
  cmv = mv;
  for mn = 1 : size(mv,2)
    if ( mv(mn) < 0 )
      cmv(mn) = 0;
    end
  end
end

% ----------------------------


% initial minion wave
m1 = [ ones(1, rmr) * rh ones(1, (ws - rmr)) * mh ];
m2 = m1; % waves are the same


for i = 1 : 56
% calculations
%ad1 = nnz(m1) * ((rd + md) / 2); % attack damage
%ad2 = nnz(m2) * ((rd + md) / 2); % attack damage
% !!!!!!! very bad calculations !!!!!!! TODO

% attack damages
ad1 = attackDamage(m1, rd, md, rmr);
ad2 = attackDamage(m2, rd, md, rmr);

% damage vectors
dv1 = damageVector(m1);
dv2 = damageVector(m2);

% waves after attack
m1 = attack(m1, ad1, dv1);
m2 = attack(m2, ad2, dv2);

% correct dead minions values to 0
m1 = correctValues(m1);
m2 = correctValues(m2);

% pick winner
if (nnz(m1) == 0 | nnz(m2) == 0)
  break;
end

end
m1
m2


% ------------------------------------------------------------------------------------------------------------
% na razie musi byc tak, ze po zakonczeniu "bitwy" zliczane sa zwyciestwa; potem pozostale przy zyciu mioniony
% powinny zostawac na polu i dolaczac do kolejnej fali

% oprocz tego kwestia wiez i buff'ow
% wieze: przy 100%, 90%, 70%, 40%, 10%
% inhibitor: 30%, daje + 70% dla melee

% n = nnz(X) returns the number of nonzero elements in matrix X.
