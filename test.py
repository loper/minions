#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minions - simulate LoL minions spawns and fights
by Loper
'''

CLIENT_VER = '0.2'
CLIENT_TYPE = 'dev' # change to PBE/prod

from Wave import Wave
from Attack import Attack
from Turret import Turret
from Login import Login
import Buff
from Runes import Runes


from time import sleep
from sys import exit
import random

# REMOVE THIS @ PROD
import pdb


# SETTINGS
sleep_time = .2
upgrade_every = 15 # upgrade every n waves
siege_every = 3 # spawn siege minion every n waves
wave_size = 6

# balance: blue [%%%%%%%%########] purple
#              0%      50%     100%
balance_typical = 50 # typical attacks per timer
balance_change = 1 # balance progress change

# waves containers
waves_blue = []
waves_purple = []

# turrets
turr_positions = [5, 20, 40, 60, 80, 95]
turr_range = 2



def debugHealths():
  blue = waves_blue
  purple = waves_purple
  blue=[k.wave for k in blue]
  purple=[k.wave for k in purple]
  z={}
  for b in blue:
    z.update(b)
  print([v.health for k,v in z.items()], end='')
  print(' / ', end = '')
  z={}
  for b in purple:
    z.update(b)
  print([v.health for k,v in z.items()])


def debugDamage():
  blue = waves_blue
  purple = waves_purple
  blue=[k.wave for k in blue]
  purple=[k.wave for k in purple]

  z={}
  for b in blue:
    z.update(b)
  damages = [v.damage for k,v in z.items()]
  magic = [v.magic_damage for k,v in z.items()]
  damages = [x+y for x,y in zip(damages, magic)]
  print(damages, end='')

  print(' / ', end = '')

  z={}
  for b in purple:
    z.update(b)
  damages = [v.damage for k,v in z.items()]
  magic = [v.magic_damage for k,v in z.items()]
  damages = [x+y for x,y in zip(damages, magic)]
  print(damages)

  print('')


def createWave(starting, cycle, siege = 0):
  """ create wave and update old """
  wave = Wave(starting)
  new_wave = wave.create(cycle = cycle, c_siege = siege)
  return wave


def updateSpawnTimes(spawn_times, balance):
  """ update spawn times """
  spawn_times[0] = round(balance_typical * balance / 100)
  spawn_times[1] = balance_typical - spawn_times[0]

# TEST - boundaries ----------
  bound = 5
  bound_add = bound - 1
  if(spawn_times[0]) <= bound:
    spawn_times[0] += bound_add
  if(spawn_times[1]) <= bound:
    spawn_times[1] += bound_add
# ----------------------------

#  spawn_times[spawn_times.index(min(spawn_times))] *= 2 # limits boundaries
#  spawn_times[spawn_times.index(max(spawn_times))] /= 2 # limits boundaries
  return spawn_times


def checkTimer(timer, blue, purple, starting):
  """ check timer and spawn wave """
  cycle = round(timer / upgrade_every)

# applyRandomBuff(timer, spawn_times, blue, purple)

  # spawn siege every n waves
  siege = 0
  if timer % siege_every == 0: siege = 1

  # create next waves
  if (spawn_times[0] == 0 or timer % spawn_times[0] == 0):
    # blue
    new_wave = createWave(starting, cycle, siege)
    waves_blue.append(new_wave)

  if (spawn_times[1] == 0 or timer % spawn_times[1] == 0):
    # purple
    new_wave = createWave(starting, cycle, siege)
    waves_purple.append(new_wave)


def applyRandomBuff(timer, spawn_times, blue, purple):
  """ applies randomly choosen buff for random wave """
  chance = 75 # %
  every = 10 # every timer tick

  # modify chance according to value
#chance += round(max(spawn_times))
  every = min(spawn_times)

  if ( timer % every ) == 0:
    rand = random.random()
    if rand <= chance/100:
      blue_chance = round(spawn_times[1]/(spawn_times[0]+spawn_times[1])*100)

      buff = Buff()
      rand = random.random()

      # pick team
      # if random.randint(1,2) == 1:
      if rand <= blue_chance/100:
        wave = blue
        waves_list = waves_blue
      else:
        wave = purple
        waves_list = waves_purple

      # pick buff
      if random.randint(1,2) == 1:
        wave=buff.applyRed(wave, waves_list)
      else:
        wave=buff.applyBlue(wave, waves_list)



def showData(w_blue, w_purple, balance):
  """ show progress """
  # wait for a while
  sleep(sleep_time)
# print('--------------- {} / {}'.format(len(waves_blue),len(waves_purple)))
# print("{}".format(balance))
# print('\r{} / {}'.format(len(waves_blue),len(waves_purple)), end='')

  # get list of buffs
  buffs_blue = []
  buffs_purple = []
  for waves in w_blue:
    buffs_blue.extend(waves.buffs)
    waves.clearBuffs()
  for waves in w_purple:
    buffs_purple.extend(waves.buffs)
    waves.clearBuffs()

#  print([wave.buffs for wave in w_blue])
# print('{} / {}'.format(len(w_blue),len(w_purple)))

  if 'blue' in buffs_blue:
    print("\033[1;34m", end = '')
  elif 'red' in buffs_blue:
    print("\033[1;31m", end = '')
  print("#" * balance, end = '')
  print("\033[0;0m", end = '')

  if 'blue' in buffs_purple:
    print("\033[1;34m", end = '')
  elif 'red' in buffs_purple:
    print("\033[1;31m", end = '')
  print("-" * (100-balance), end='')
  print("\033[0;0m", end = '')

  print('\r', end = '')



if __name__ == '__main__':

  # create initial waves
  starting = 1
  wave_blue = Wave(starting)
  blue = wave_blue.create()
  wave_purple = Wave(len(blue.wave)+starting)
  purple = wave_purple.create()
  waves_blue.append(blue)
  waves_purple.append(purple)

  debugHealths()
  print('')
  # ---------------------------

  login = Login(CLIENT_VER, CLIENT_TYPE)
  user_runes = login.loadRunes()

  runes = Runes()
  runes.prepareData(login.connection, user_runes)
  runes.applyRunes(blue, 3)

  pdb.set_trace()

  debugHealths()
  print('')

  login.disconnect()
  exit()


  turr = Turret()
  turrets = turr.createTurrets(turr_positions, turr_range)


  # timer per attack
  timer = 0

  # create initial waves
  starting = 1
  wave_blue = Wave(starting)
  blue = wave_blue.create()
  wave_purple = Wave(len(blue.wave)+starting)
  purple = wave_purple.create()

  waves_blue.append(blue)
  waves_purple.append(purple)

  debugHealths()

  for i in range(1,9):
    turr.attack(turrets, waves_blue, waves_purple, 40)
    turr.scaleAll(turrets,60)
    debugHealths()

  for i in range(1,9):
    turr.attack(turrets, waves_blue, waves_purple, 22)
    debugHealths()

  for i in range(1,9):
    turr.attack(turrets, waves_blue, waves_purple, 5)
    debugHealths()
