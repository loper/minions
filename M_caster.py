#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minion - class defining caster minion stats
'''

from Minion import Minion


class M_caster(Minion):

  def __init__(self, m_id = 0, cycle = 0, cyc_divider = 2):
    """ initiate default values """
    super().__init__(m_id, cycle)
    self.m_id = m_id
    self.type = 'caster'

    self.health = 280
    self.damage = 0
    self.magic_damage = 25
    self.armor = 0 # %
    self.magic_resist = 3 # %

    self.crit_chance = 0  # %
    self.exposed = 0.4  # %

    # cycles
    self.c_health = 6
    self.c_damage = 0
    self.c_magic_damage = 3
    self.c_armor = 0.45
    self.c_magic_resistance = 1.1
    if cycle > 0:
      self.setCycle(cycle/cyc_divider-1)

