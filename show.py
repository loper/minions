# w_blue = [<Wave.Wave object at 0x7fa436c3edd8>]
# turrets = [<Turret.Turret object at 0x7fa436c5c978>, <Turret.Turret object at 0x7fa436c5c748>, <Turret.Turret object at 0x7fa436c5c8d0>, <Turret.Turret object at 0x7fa436c5c9b0>, <Turret.Turret object at 0x7fa436c5ca20>, <Turret.Turret object at 0x7fa436c5ca90>]


def showData(w_blue, w_purple, balance, turrets, timer):
  """ show progress """

# print('--------------- {} / {}'.format(len(waves_blue),len(waves_purple)))
# print("{}".format(balance))
# print('\r{} / {}'.format(len(waves_blue),len(waves_purple)), end='')

#  debugHealths()
# debugArmor()
#  damages = [v.damage for v in turrets]
#  magic = [v.magic_damage for v in turrets]
#  damages = [x+y for x,y in zip(damages, magic)]
#  print(damages)
#  print('')
# return

  # get list of buffs
  buffs_blue = []
  buffs_purple = []
  for waves in w_blue:
    buffs_blue.extend([w.name for w in waves.buffs])
  for waves in w_purple:
    buffs_purple.extend([w.name for w in waves.buffs])

  # TODO: pobieranie koloru buffa dynamicznie
  if 'silver' in buffs_blue:
    print('\033[1;30m\033[47m', end ='')
  elif 'blue' in buffs_blue:
    print("\033[1;34m", end = '')
  elif 'red' in buffs_blue:
    print("\033[1;31m", end = '')
  print("#" * balance, end = '')
  print("\033[0;0m", end = '')

  if 'blue' in buffs_purple:
    print("\033[1;34m", end = '')
  elif 'red' in buffs_purple:
    print("\033[1;31m", end = '')
  print("-" * (100-balance), end='')
  print("\033[0;0m", end = '')

  # show 'Y' signs where turrets occur
  for turret_p in turrets:
    print("\033[1;36m\033[{}GY\033[0;0m".format(turret_p.position), end='')
  print("\033[105G", end='')
  print(' {} | {}'.format(len(w_blue),len(w_purple)), end='')
  print(' ({})'.format(timer), end='')
# print(' '*20, end='')
  print('\r', end = '')


