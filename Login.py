#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Login - class defining login into database
'''

from user import username, password
from Sql import Base

from sys import exit
from random import random
import socket


class Login(object):

    def __init__(self, client_ver, client_type):
        """ initiate default values """
        host = 'vitalic'
        db = 'minions_{0}'.format(client_type)
        static = 'minions+'
        self.hostname = socket.gethostname()

        # connect to DB
        try:
            self.connection = Base(host, username, static+password, db)
        except:
            print('[ERROR] Bad login, password or permissions to database')
            exit(8)

        # check client version
        self.checkVersion(client_ver, client_type)

        # get user info
        self.username = username
        user_info = self.getUserInfo(username, self.hostname)
        if len(user_info) < 1:
            print('[ERROR] User not found in database')
            exit(9)
        self.user_id = user_info[0][0]
        self.status = user_info[0][4]

        # check account status
        if self.status != 1:
            print('[ERROR] Your account has been blocked')
            exit(12)

        # get user points
        self.points = self.getUserPoints(self.user_id)

        # set user team (random)
        self.team = 'blue' if random() < 0.5 else 'purple'
        self.team_color = '\033[1;34m' if self.team == 'blue' else '\033[1;35m'

        # get user league
        self.league = self.getLeague()

        # show info
        print('Logged as {0}, total points: \033[1;33m{1}\033[0;0m'.format(
            self.username.upper(), self.points))
        if self.league is not None:
            print('Your league is {}\n'.format(self.league))
        print('You are in {}{}{} team'.format(self.team_color,
                                              self.team, '\033[0;0m'))
        print('')

    def getUserInfo(self, user, hostname):
        """ get user information from database """
        query = "SELECT * FROM users WHERE user='{0}' AND host='{1}'".format(
            user, hostname)
        data = self.connection.executeQuery(query)
        return data

    def checkVersion(self, client_version, client_type):
        """ check client version according to server """
        query = "SELECT version FROM version WHERE type = '{0}' \
                         ORDER BY created DESC LIMIT 1;".format(client_type)
        version = self.connection.executeQuery(query)
        if len(version) < 1:
            print('[ERROR] Error getting version from database')
            exit(10)
        if version[0][0] != client_version:
            print('[ERROR] Please update your client to the newest \
                  version {}'.format(version[0][0]))
            exit(11)

    def getUserPoints(self, user_id):
        """ get amount of points for the user """
        query = "SELECT total FROM points WHERE user_id = {0}".format(user_id)
        points = self.connection.executeQuery(query)
        if len(points) < 1:
            print('[ERROR] Error getting points from database')
            print(query)
            exit(13)
        return points[0][0]

    def updatePoints(self, points):
        """ set amount of points in database """
        actual = self.points
        added = points[0] if self.team == 'blue' else points[1]
        total = actual + added
        query = "UPDATE points SET total = {0} WHERE user_id = {1} \
            LIMIT 1".format(total, self.user_id)
        self.connection.executeQuery(query, False)

    def addMatch(self, points, balance, timer):
        """ add match info to database """
        query = "INSERT INTO `match` (date, blue_points, purple_points, \
            balance, timer) VALUES (now(),{0},{1},{2},{3});".format(
                points[0], points[1], balance, timer)
        self.connection.executeQuery(query, False)

    def showResult(self, balance):
        """ show match result """
        result = '\033[1;31m~~~ DEFEAT! ~~~\033[0;0m'

        if self.team == 'blue' and balance > 90 or \
                self.team == 'purple' and balance < 10:
            result = '\033[1;32m~~~ VICTORY! ~~~\033[0;0m'

        print(result)

    def loadRunes(self):
        """ get rune list from DB """
        query = "SELECT type, name, amount FROM runes WHERE owner = {};".format(
            self.user_id)
        data = self.connection.executeQuery(query)
        return data

    def getLeague(self):
        """ get league for user """
        data = self.connection.executeProc('getLeague', str(self.user_id))[0][0]

        # unranked?
        if 'unranked' in data:
            return None

        # case color
        if 'bronze' in data:
            color = '0;31m'
        elif 'silver' in data:
            color = '1;37m'
        elif 'gold' in data:
            color = '1;33m'
        elif 'platinium' in data:
            color = '0;34m'
        elif 'diamond' in data:
            color = '1;36m'
        elif 'master' in data:
            color = '1;32m'
        else:
            color = '0;0m'

        return '\033[{}{}\033[0;0m'.format(color, data)

    def disconnect(self):
        """ disconnect from DB """
        self.connection.disconnect()
