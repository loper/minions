#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Turret - class defining defending turrets
'''

from Attack import Attack
import itertools

import pdb


class Turret(object):

  def __init__(self):
    """ initiate default values """
    self.health = 8700
    self.damage = 552
    self.magic_damage = 390
    self.armor = 27 # %
    self.magic_resistance = 15 # %
    self.crit_chance = 20 # %

    self.position = 0
    self.ranges = []

    # for compatibility with Attack
    self.exposed = 1
    self.m_id = 0

    # cycles
    self.c_damage = 97
    self.c_magic_damage = 75
    self.c_armor = 1.5
    self.c_magic_resistance = 1.25

    # correlated classes
    self.att = Attack()


  def calculateRanges(self, position, radius):
    """ calculates list of positions in range (AoE) """
    rad = range(position-radius, position+radius+1)
    all_radius = [val for val in rad]
    all_radius.sort()
    return all_radius


  def createTurrets(self, positions, radius):
    """ create all the turrets """
    turrets = []
    for pos in positions:
      new_turret = self.__class__()
      new_turret.position = pos
      new_turret.ranges = self.calculateRanges(pos, radius)
      turrets.append(new_turret)
    return turrets


  def attack(self, turrets, blue_wave, purple_wave, balance):
    """ attack wave by turret """
    blue_range = [turr for turr in turrets if turr.position > 50]
    purple_range = [turr for turr in turrets if turr.position < 50]

    if len(blue_range) == 0 and len(purple_range) == 0: return # no turrets

    if len(blue_range) > 0:
      blue_range = blue_range[0]
    if len(purple_range) > 0:
      purple_range = purple_range[-1]

    wave = None
    if type(blue_range) is Turret and balance in blue_range.ranges:
      wave = blue_wave
      turret = blue_range
    elif type(purple_range) is Turret and balance in purple_range.ranges:
      wave = purple_wave
      turret = purple_range

    if wave is not None:
      # in range of a turret
      minions_list = {}
      for minions in wave:
        minions_list.update(minions.wave)

      for i in range(0, len(minions_list)):
        # remove destroyed turrets
        if turret.health <= 0:
          turrets.remove(turret)
          del(turret)
          self.att.remove_corpses(wave)
          return True
        # turret attacks minions
        tmp2, wave2 = self.att.one_battle({0: turret}, minions_list, i)
        # minions attack turret only when they're in range and opponent wave is clear
        if (wave == blue_wave and len(purple_wave) == 0) \
           or (wave == purple_wave and len(blue_wave) == 0):
          # minions attack turret
          tmp3, wave3 = self.att.one_battle(minions_list, {0: turret}, 0)

      self.att.remove_corpses(wave)
      return False
    else:
      return True


  def nextCycle(self):
    """ scaling per every wave """
    self.damage += self.c_damage
    self.magic_damage += self.c_magic_damage
    self.armor += self.c_armor
    if self.armor > 100:
      self.armor = 100
    self.magic_resistance += self.c_magic_resistance
    if self.magic_resistance > 100:
      self.magic_resistance = 100


  def scaleAll(self, turrets_list):
    """ scale all turrets in list """
    for turret in turrets_list:
      turret.nextCycle()

