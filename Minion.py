#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
Minion - class defining minion stats (default: melee)
'''

from copy import copy


class Minion(object):

  def __init__(self, m_id, cycle = 0, cyc_divider = 2):
    """ initiate default values """
    self.m_id = m_id
    self.type = 'melee'

    self.health = 445
    self.damage = 12
    self.magic_damage = 0
    self.armor = 8 # %
    self.magic_resistance = 0 # %
    self.max_resist = 80 # %

    self.crit_chance = 0  # % - deals 2x damage
    self.exposed = 1  # %

    # cycles
    self.c_health = 7
    self.c_damage = 2
    self.c_magic_damage = 0
    self.c_armor = 1.2
    self.c_magic_resistance = 0.65
    if cycle > 0:
      self.setCycle(cycle/cyc_divider-1)
    self.cycle = cycle



  def create(self, count = 1):
    """ create a set of minions """
    result = {}

    for i in range(0, count):
      # create new instance
      minion = self.__class__(self.m_id + i, self.cycle)
      if minion.m_id in result:
        print('--------- error 01: duplicate m_id ----------')
      # insert in array
      result.update( {minion.m_id : minion} )
    return result


  def setCycle(self, cycle):
    """ scaling per every wave """
    self.health += self.c_health * cycle
    self.damage += self.c_damage * cycle
    self.magic_damage += self.c_magic_damage * cycle
    self.armor += self.c_armor * cycle
    self.armor = min(self.armor, self.max_resist)
    self.magic_resistance += self.c_magic_resistance * cycle
    self.magic_resistance = min(self.magic_resistance, self.max_resist)

