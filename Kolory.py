#-*- coding: utf-8 -*-
'''Umożliwia kolorowanie wyjścia konsoli'''

import logging

class Kolor(object):
    '''klasa Kolor'''

    def __init__(self, konfiguracja, czy_kolorowe = True):
        '''włącza lub wyłącza kolorowanie'''
        if czy_kolorowe:
            self.wlacz_kolory()
        else:
            self.wylacz_kolory()
        '''zapisanie obiektu konfiguracji, z którego będą brane
        poszczególne ustawienia kolorów'''
        self.konfiguracja = konfiguracja

    def wlacz_kolory(self):
        '''włączenie kolorowania wyjścia'''
        # TODO: zrobić to w słowniku, a nie jako atrybuty klasy
        # TODO: attribute 'blue,yellow,...)' defined outside __init__
        self.norm = '\033[1;m'
        self.yellow = '\033[1;33m'
        self.red = '\033[1;31m'
        self.hred = '\033[1;41m'
        self.green = '\033[1;32m'
        self.magenta = '\033[1;35m'
        self.white = '\033[1;37m'
        self.cyan = '\033[1;36m'
        self.blue = '\033[1;34m'
        self.grey = '\033[1;30m'

    def wylacz_kolory(self):
        '''wyłączenie kolorów - puste wartości'''
        self.norm = ''
        self.yellow = ''
        self.red = ''
        self.hred = ''
        self.green = ''
        self.magenta = ''
        self.white = ''
        self.cyan = ''
        self.blue = ''
        self.grey = ''

    def koloruj(self, tekst, obiekt):
        '''koloruje podany tekst na podstawie pobranych ustawień z pliku
        konfiguracyjnego - opcja k_<obiekt>'''
        try:
            kolor_ustawienia = self.konfiguracja.podaj_wartosc(
                                                    "k_{}".format(obiekt))
            kolor_tekstu = getattr(self, kolor_ustawienia)
            return self.formatuj(tekst, kolor_tekstu)
        except AttributeError:
            logging.error(self.koloruj("[{}] Error: {}: k_{}".format('Kolor',
                'Setting not found', obiekt), 'blad'))
            return tekst


    def formatuj(self, tekst, kolor_tekstu):
        '''koloruje tekst podanym kolorem - kolor typu self.<kolor>'''
        return "{}{}{}".format(kolor_tekstu, tekst, self.norm)

    def pokaz_wszystkie(self):
        '''lista kolorów, która może się przydać przy tworzeniu modułu 
        lub konfiguracji'''
        print('\033[1;30mGray like Ghost\033[1;m')
        print('\033[1;31mRed like Radish\033[1;m')
        print('\033[1;32mGreen like Grass\033[1;m')
        print('\033[1;33mYellow like Yolk\033[1;m')
        print('\033[1;34mBlue like Blood\033[1;m')
        print('\033[1;35mMagenta like Mimosa\033[1;m')
        print('\033[1;36mCyan like Caribbean\033[1;m')
        print('\033[1;37mWhite like Whipped Cream\033[1;m')
        print('\033[1;38mCrimson like Chianti\033[1;m')
        print('\033[1;41mHighlighted Red like Radish\033[1;m')
        print('\033[1;42mHighlighted Green like Grass\033[1;m')
        print('\033[1;43mHighlighted Brown like Bear\033[1;m')
        print('\033[1;44mHighlighted Blue like Blood\033[1;m')
        print('\033[1;45mHighlighted Magenta like Mimosa\033[1;m')
        print('\033[1;46mHighlighted Cyan like Caribbean\033[1;m')
        print('\033[1;47mHighlighted Gray like Ghost\033[1;m')
        print('\033[1;48mHighlighted Crimson like Chianti\033[1;m')
